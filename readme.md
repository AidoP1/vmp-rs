# VMP [![Build Status](https://gitlab.com/AidoP1/vmp-rs/badges/master/build.svg)](https://gitlab.com/AidoP1/vmp-rs/pipelines)

A crate for reading and writing generic vector collections.

Vectors with up to 255 components and with datatypes between 2^3 and 2^10 bits long are supported by this crate utilising Rust's powerful generics.

Note that this crate no longer supports packing types smaller than a byte as the process is too complex and expensive to do generically.

## To-do List

- [x] Correctly serialize types, accounting for endian-ness
- [ ] Documentation
- [ ] More unit tests
- [ ] Implement `Packable` for all primitives
- [ ] Ensure everything is appropriately checked

## Usage

Add
```
[dependencies]
vmp = "0.0.1"
```
to your `Cargo.toml`

Requires nightly to compile

### Opening VMP files

First, implementing the `Vector` trait on your vector

```rust
extern crate vmp;
use vmp::*;

// You should already have something like this
#[derive(Debug)]
struct Vector3<T: Packable> {
    x: T,
    y: T,
    z: T
}

impl<T: Packable> Vector for Vector3<T> {
    const COMPONENTS: usize = 3;    // Number of components. Used by the open and write functions
    type Type = T;

    fn construct(components: &[Self::Type]) -> Self {
        Self {
            x: components[0],
            y: components[1],
            z: components[2]
        }
    }

    fn deconstruct(&self) -> Box<[Self::Type]> {
        vec![self.x, self.y, self.z].into_boxed_slice()
    }
}

impl<T: Packable> Default for Vector3<T> {
    fn default() -> Self {
        Self {
            x: T::default(),
            y: T::default(),
            z: T::default()
        }
    }
}
```

Then, you simply ask to open a file, specifying which `Vector` and datatype to use.

```rust
fn main() -> io::Result<()> {
    let vmp = vmp::VectorMap::<Vector3<u8>>::open("file.vmp")?;
    println!("First vector is: {:#?}", vmp.map[0]);

    Ok(())
}
```

### Writing VMP files

As above, you first need your vector to implement the `Vector` trait.

Then it is as easy as
```rust
fn main() {
    let vectors: Vec<Vector3<u8>> = get_vec_of_vectors();

    // Create a vector map from a Vec<_>
    let vector_map: VectorMap<Vector3<u8>> = VectorMap::create(vectors);
    // Second argument specifies if you want to include a checksum to watch for file corruption
    vector_map.write("file.vmp", false);
}
```