use vmp::*;

#[derive(Debug)]
#[derive(Clone)]
struct Vector2<T: Packable> {
    x: T,
    y: T
}

impl<T: Packable> Vector2<T> {
    pub fn new(x: T, y: T) -> Self {
        Self {
            x, y
        }
    }
}

impl<T: Packable> Vector for Vector2<T> {
    const COMPONENTS: usize = 2;
    type Type = T;

    fn construct(components: &[Self::Type]) -> Self {
        Self {
            x: components[0],
            y: components[1]
        }
    }

    fn deconstruct(&self) -> Box<[Self::Type]> {
        vec![self.x, self.y].into_boxed_slice()
    }
}

impl<T: Packable> Default for Vector2<T> {
    fn default() -> Self {
        Self {
            x: T::default(),
            y: T::default()
        }
    }
}

impl<T: Eq> Eq for Vector2<T> where T: Packable {}

impl<T: PartialEq> PartialEq for Vector2<T> where T: Packable {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}

#[derive(Debug)]
#[derive(Clone)]
struct Vector3<T: Packable> {
    x: T,
    y: T,
    z: T
}

impl<T: Packable> Vector3<T> {
    pub fn new(x: T, y: T, z: T) -> Self {
        Self {
            x, y, z
        }
    }
}

impl<T: Packable> Vector for Vector3<T> {
    const COMPONENTS: usize = 3;
    type Type = T;

    fn construct(components: &[Self::Type]) -> Self {
        Self {
            x: components[0],
            y: components[1],
            z: components[2]
        }
    }

    fn deconstruct(&self) -> Box<[Self::Type]> {
        vec![self.x, self.y, self.z].into_boxed_slice()
    }
}

impl<T: Packable> Default for Vector3<T> {
    fn default() -> Self {
        Self {
            x: T::default(),
            y: T::default(),
            z: T::default()
        }
    }
}

impl<T: Eq> Eq for Vector3<T> where T: Packable {}

impl<T: PartialEq> PartialEq for Vector3<T> where T: Packable {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y && self.z == other.z
    }
}

#[test]
fn test_open_8x2() {
    let vmp: VectorMap<Vector2<u8>> = VectorMap::open("tests/8x2.vmp").unwrap();
    assert_eq!(vmp.map.to_vec(), vec![Vector2::new(31, 66), Vector2::new(139, 255), Vector2::new(11, 42), Vector2::new(18, 0), Vector2::new(99, 237)])
}

#[test]
fn test_open_16x3() {
    let vmp: VectorMap<Vector3<i16>> = VectorMap::open("tests/16x3.vmp").unwrap();
    assert_eq!(vmp.map.to_vec(), vec![Vector3::new(6969, -2, 8008), Vector3::new(-420, -11250, 0), Vector3::new(22853, 17748, -999)])
}

#[test]
fn test_write_16x2() {
    let vmp: VectorMap<Vector2<i16>> = VectorMap::create(vec![Vector2::new(-171, 110), Vector2::new(-51, 4269), Vector2::new(350, -1388), Vector2::new(23478, 0), Vector2::new(-0, 0xFFFFu16 as i16)]);
    vmp.write("tests/write_16x2.vmp", false).unwrap();

    // Now open to test the write process
    let vmp: VectorMap<Vector2<i16>> = VectorMap::open("tests/write_16x2.vmp").unwrap();
    assert_eq!(vmp.map.to_vec(), vec![Vector2::new(-171, 110), Vector2::new(-51, 4269), Vector2::new(350, -1388), Vector2::new(23478, 0), Vector2::new(0, -1)])
}

#[test]
fn test_write_buffer_16x2() {
    let buffer = vec![255, 10, 11, 24, 198, 42, 69, 69].into_boxed_slice();
    vmp::VectorMap::<Vector2<i16>>::write_buffer("tests/write_buffer_16x2.vmp", &buffer, false).unwrap();

    // Now open to test the write process
    let vmp: VectorMap<Vector2<i16>> = VectorMap::open("tests/write_buffer_16x2.vmp").unwrap();
    assert_eq!(vmp.map.to_vec(), vec![Vector2::new(255 << 8 | 10, 11 << 8 | 24), Vector2::new(198 << 8 | 42, 69 << 8 | 69)])
}