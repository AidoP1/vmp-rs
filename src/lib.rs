pub trait Vector: Default + Clone {
    const COMPONENTS: usize;
    type Type: Packable;

    /// Create vector from components
    fn construct(components: &[Self::Type]) -> Self;
    fn deconstruct(&self) -> Box<[Self::Type]>;

   // /// The `n`th root of the sum of the `n` squared components
    //fn magnitude(&self) -> f64;


}

#[derive(PartialEq)]
pub enum PackableType { Signed = 0, Unsigned = 1, Float = 2, Character = 3, Other = 4, Small = 5 }
impl PackableType {
    fn from(raw: u8) -> Self {
        match raw {
            0 => PackableType::Signed,
            1 => PackableType::Unsigned,
            2 => PackableType::Float,
            3 => PackableType::Character,
            5 => PackableType::Small,
            _ => PackableType::Other
        }
    }
}

pub trait Packable: Default + Copy + Clone {
    const SIZE: usize;
    const TYPE: PackableType;

    fn pack(self) -> Box<[u8]>;                 // Could be completely generic if there were a trait for `to_be_bytes`
    fn unpack(bytes: &[u8]) -> Option<Self>; 
}

impl Packable for u8 {
    const SIZE: usize = 1;
    const TYPE: PackableType = PackableType::Unsigned;

    fn pack(self) -> Box<[u8]> {
        Box::new(self.to_be_bytes())
    }
    
    fn unpack(bytes: &[u8]) -> Option<Self> {
        if let Some(byte) = bytes.first() {
            Some(u8::from_be_bytes([*byte]))
        } else {
            None
        }
    }
}

impl Packable for i8 {
    const SIZE: usize = 1;
    const TYPE: PackableType = PackableType::Signed;

    fn pack(self) -> Box<[u8]> {
        Box::new(self.to_be_bytes())
    }
    
    fn unpack(bytes: &[u8]) -> Option<Self> {
        if let Some(byte) = bytes.first() {
            Some(i8::from_be_bytes([*byte]))
        } else {
            None
        }
    }
}

impl Packable for u16 {
    const SIZE: usize = 2;
    const TYPE: PackableType = PackableType::Unsigned;

    fn pack(self) -> Box<[u8]> {
        Box::new(self.to_be_bytes())
    }
    
    fn unpack(bytes: &[u8]) -> Option<Self> {
        if bytes.len() == Self::SIZE {
            Some(u16::from_be_bytes([bytes[0], bytes[1]]))
        } else {
            None
        }
    }
}

impl Packable for i16 {
    const SIZE: usize = 2;
    const TYPE: PackableType = PackableType::Signed;

    fn pack(self) -> Box<[u8]> {
        Box::new(self.to_be_bytes())
    }
    
    fn unpack(bytes: &[u8]) -> Option<Self> {
        if bytes.len() == Self::SIZE {
            Some(Self::from_be_bytes([bytes[0], bytes[1]]))
        } else {
            None
        }
    }
}

impl Packable for f32 {
    const SIZE: usize = 4;
    const TYPE: PackableType = PackableType::Float;

    fn pack(self) -> Box<[u8]> {
        Box::new(self.to_be_bytes())
    }
    
    fn unpack(bytes: &[u8]) -> Option<Self> {
        if bytes.len() == Self::SIZE {
            Some(Self::from_be_bytes([bytes[0], bytes[1], bytes[2], bytes[3]]))
        } else {
            None
        }
    }
}

impl Packable for f64 {
    const SIZE: usize = 8;
    const TYPE: PackableType = PackableType::Float;

    fn pack(self) -> Box<[u8]> {
        Box::new(self.to_be_bytes())
    }
    
    fn unpack(bytes: &[u8]) -> Option<Self> {
        if bytes.len() == Self::SIZE {
            Some(Self::from_be_bytes([bytes[0], bytes[1], bytes[2], bytes[3], bytes[4], bytes[5], bytes[6], bytes[7]]))
        } else {
            None
        }
    }
}

/// Should pack/unpack like u64/u32/u16 etc
impl Packable for usize {
    const SIZE: usize = std::mem::size_of::<Self>();
    const TYPE: PackableType = PackableType::Unsigned;

    fn pack(self) -> Box<[u8]> {
        Box::new(self.to_be_bytes())
    }
    
    fn unpack(bytes: &[u8]) -> Option<Self> {
        if bytes.len() == Self::SIZE {
            Some(Self::from_be_bytes(
                #[cfg(target_pointer_width = "16")]
                {
                    [bytes[0], bytes[1]]
                },
                #[cfg(target_pointer_width = "32")]
                {
                    [bytes[0], bytes[1], bytes[2], bytes[3]]
                },
                #[cfg(target_pointer_width = "64")]
                {
                    [bytes[0], bytes[1], bytes[2], bytes[3], bytes[4], bytes[5], bytes[6], bytes[7]]
                },
                #[cfg(not(any(target_pointer_width = "64", target_pointer_width = "32", target_pointer_width = "16")))]
                {
                    unimplemented!()
                }
            ))
        } else {
            None
        }
    }
}

/// Should pack/unpack like i64/i32/i16 etc
impl Packable for isize {
    const SIZE: usize = std::mem::size_of::<Self>();
    const TYPE: PackableType = PackableType::Signed;

    fn pack(self) -> Box<[u8]> {
        Box::new(self.to_be_bytes())
    }
    
    fn unpack(bytes: &[u8]) -> Option<Self> {
        if bytes.len() == Self::SIZE {
            Some(Self::from_be_bytes(
                #[cfg(target_pointer_width = "16")]
                {
                    [bytes[0], bytes[1]]
                },
                #[cfg(target_pointer_width = "32")]
                {
                    [bytes[0], bytes[1], bytes[2], bytes[3]]
                },
                #[cfg(target_pointer_width = "64")]
                {
                    [bytes[0], bytes[1], bytes[2], bytes[3], bytes[4], bytes[5], bytes[6], bytes[7]]
                },
                #[cfg(not(any(target_pointer_width = "64", target_pointer_width = "32", target_pointer_width = "16")))]
                {
                    unimplemented!()
                }
            ))
        } else {
            None
        }
    }
}

use std::path::Path;
use std::fs::File;
use std::io::{
    Result,
    Read,
    Write,
    Error,
    ErrorKind
};

/**
 * A Vector Map is an array of fixed type and size collections
 * For example, an image is an array of colours which is a collection of 3 or 4 u8's.
 * 
 * This library provides means to load and write these arrays for various collection size and types
 * 
 * # On Disk
 * typically prefixed by .vmp, 
 * 
 * -    4 byte VMP\0
 * -    32 bit integer for number of vectors
 * -    8 bit integer for number of channels
 * -    3 bit `depth` of type. This value means the type uses 2^3+n bits (8, 16, 32, 64, 128 .. 1024). If `type` is of small-other (6), depth is 8 but only lower n will be used
 * -    3 bit `type` of type. Values: 0=signed, 1=unsigned, 2=float, 3=character (utf) or string, 4=other (unchecked, may be custom serializable type), 5=small-other, 6 & 7 = reserved
 * -    1 bit `rectangle`. Indicates that the 32 bit number should be interpreted as 16 bit width * height 
 * -    1 bit `checked`. If set a checksum immideately follows
 * -    64 bits (optional) checksum. 
 * 
 */

#[derive(Debug)]
pub enum Size {
    Length(u32),
    Dimensions(u16, u16)
}

impl Size {
    pub fn magnitude(&self) -> u32 {
        match self {
            Self::Length(value) => *value,
            Self::Dimensions(width, height) => *width as u32 * *height as u32
        }
    }

    fn to_bytes(self) -> [u8; 4] {
        match self {
            Self::Length(value) => value.to_be_bytes(),
            Self::Dimensions(width, height) => {
                let width = width.to_be_bytes();
                let height = height.to_be_bytes();
                [width[0], width[1], height[0], height[1]]
            }
        }
    }

    /// Lazily unwrap the size so that width is either the width or total size
    pub fn lazy_width(&self) -> u32 {
        match self {
            Self::Length(value) => *value,
            Self::Dimensions(width, _) => *width as u32
        }
    }

    /// Lazily unwrap the size so that height is either the height or 1 if unknown
    pub fn lazy_height(&self) -> u16 {
        match self {
            Self::Dimensions(_, height) => *height,
            _ => 1
        }
    }
}

#[derive(Debug)]
pub struct VectorMap<V: Vector> {
    pub map: Box<[V]>,
    pub size: Size
}

impl<V: Vector> VectorMap<V> {
    pub fn open<P: AsRef<Path>>(path: P) -> Result<Self> {
        let (size, buffer) = Self::open_buffer(path)?;
        if let Some(vector_map) = Self::transmute(size, &buffer) {
            Ok(vector_map)
        } else {
            Err(Error::new(ErrorKind::InvalidData, "failed to parse file as a Vector Map"))
        }
    }

    // A safe and checked mutation from raw bytes to vector array
    pub fn transmute(size: Size, slice: &[u8]) -> Option<Self> {
        // Ensure that there is enough bytes in slice to construct an appropriate length vector map
        if slice.len() < size.magnitude() as usize * V::COMPONENTS * V::Type::SIZE { return None };

        let mut vectors = vec![V::default(); size.magnitude() as usize].into_boxed_slice();
        let mut components = vec![V::Type::default(); V::COMPONENTS].into_boxed_slice();
        let mut vector: usize = 0;
        let mut component: usize;

        // Split into each vector
        // Aligned types require less work
        for raw_vector in slice.chunks_exact(V::COMPONENTS * V::Type::SIZE) {
            component = 0;
            for raw_component in raw_vector.chunks_exact(V::Type::SIZE) {
                if let Some(unpacked) = V::Type::unpack(raw_component) {
                    components[component] = unpacked
                } else {
                    return None
                }
                component += 1;
            }

            // Ensure we got enough components
            if components.len() == V::COMPONENTS {
                vectors[vector] = V::construct(&components)
            } else {
                return None
            }
            vector += 1;
        }

        Some(Self {
                map: vectors,
                size,
        })
    }

    pub fn create(vec: Vec<V>) -> Self {
        Self {
            size: Size::Length(vec.len() as u32),
            map: vec.into_boxed_slice()
        }
    }

    pub fn write<P: AsRef<Path>>(self, path: P, checksum: bool) -> Result<()> {
        let mut file = File::create(path)?;

        let buffer = {
            
            let is_dimensions = match self.size { Size::Length(_) => 0, Size::Dimensions(_, _) => 1 };
            let length = self.size.to_bytes();
            let mut header = vec![
                b'V', b'M', b'P', 0,                        // Identifier
                length[0], length[1], length[2], length[3], // Length (u32)
                V::COMPONENTS as u8,                        // Channels
                (V::Type::SIZE as u8 - 1) << 5 |            // Depth top 3 bits
                (V::Type::TYPE as u8) << 2 |                // Type next 3 bits
                (is_dimensions) << 1 |
                if checksum { 1 } else { 0 }
            ];
            header.append(
                &mut self.map.into_iter().map(
                    |vector| vector.deconstruct().iter().map(
                        |component| component.pack().to_vec()
                    ).flatten().collect::<Vec<u8>>()
                ).flatten().collect::<Vec<u8>>()
            );
            header
        };

        file.write_all(&buffer)?;

        Ok(())
    }

    pub fn open_buffer<P: AsRef<Path>>(path: P) -> Result<(Size, Box<[u8]>)> {
        let mut file = File::open(path)?;

        let header: [u8; 10] = {
            let mut buffer = [0; 10];
            file.read_exact(&mut buffer)?;
            buffer
        }; // Rebind as immutable

        if header[..4] != [b'V', b'M', b'P', 0] { return Err(Error::new(ErrorKind::InvalidData, "file header inappropriate")) };

        let is_dimensions = (header[9] & 0b10u8) == 0b10u8;
        let size = if is_dimensions { Size::Dimensions(u16::from_be_bytes([header[4], header[5]]), u16::from_be_bytes([header[6], header[7]])) } else { Size::Length(u32::from_be_bytes([header[4], header[5], header[6], header[7]])) };

        let channels = u8::from_be(header[8]);
        let datatype = PackableType::from(u8::from_be((header[9] & 0b11100u8 ) >> 2));
        let depth = u8::from_be(((header[9] & 0b11100000u8) >> 5) + 1);
        let checked = (header[9] & 0b1u8) == 0b1u8;

        // Check that the data is what we expect
        if channels as usize != V::COMPONENTS || depth as usize != V::Type::SIZE || datatype != V::Type::TYPE { return Err(Error::new(ErrorKind::InvalidData, "file does not contain expected values")) };

        let mut array: Box<[u8]> = vec![0; V::COMPONENTS * V::Type::SIZE * size.magnitude() as usize].into_boxed_slice();
        {   // read_to_end takes a vector, but we don't need mutability
            let mut count = 0;
            while count < array.len() {
                match file.read(&mut array[count..]) {
                    Ok(0) => return Err(Error::new(ErrorKind::UnexpectedEof, "file shorter than specified in header")),
                    Ok(n) => count += n,
                    Err(ref e) if e.kind() == ErrorKind::Interrupted => continue,
                    Err(e) => return Err(e)
                }
            }
        }

        Ok((size, array))
    }

    pub fn write_buffer<P: AsRef<Path>>(path: P, buffer: &[u8], checksum: bool) -> Result<()> {
        let mut file = File::create(path)?;

        let size = ((buffer.len() / (V::COMPONENTS * V::Type::SIZE)) as u32).to_be_bytes();
        // Future reference if we wanted a variant that allowed specifying height / width
        //if (buffer.len() / (V::COMPONENTS * V::Type::SIZE)) as u32 == size.magnitude() { size.to_bytes() } else { return Err(Error::new(ErrorKind::InvalidInput, "Buffer doesn't have specified size")) };

        let header = vec![
            b'V', b'M', b'P', 0,                // Identifier
            size[0], size[1], size[2], size[3], // Length (u32)
            V::COMPONENTS as u8,                // Channels
            (V::Type::SIZE as u8 - 1) << 5 |    // Depth top 3 bits
            (V::Type::TYPE as u8) << 2 |        // Type next 3 bits
            if checksum { 1 } else { 0 }
        ];

        file.write_all(&header)?;
        file.write_all(&buffer)?;

        Ok(())
    }

    /// Change the internal size. Will return an Err(unchanged self) if checks fail
    pub fn to_size(mut self, size: Size) -> std::result::Result<Self, Self> {
        if self.map.len() == size.magnitude() as usize {
            self.size = size;
            Ok(self)
        } else {
            Err(self)
        }
    }
}